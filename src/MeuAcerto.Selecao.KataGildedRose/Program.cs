﻿using System;
using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
	class Program
	{
		public static void Main(string[] args)
		{
			IList<Item> itens = new List<Item>{
				ItemFactory.Create("Corselete +5 DEX",10, 20, TipoItem.COMUM),
				ItemFactory.Create("Queijo Brie Envelhecido",2, 0, TipoItem.ENVELHECIDO),
				ItemFactory.Create("Elixir do Mangusto",5, 7, TipoItem.COMUM),
				ItemFactory.Create("Dente do Tarrasque",0, 80, TipoItem.LENDARIO),
				ItemFactory.Create("Dente do Tarrasque",-1, 80, TipoItem.LENDARIO),
				ItemFactory.Create(
					 "Ingressos para o concerto do Turisas",
					15,
					20,
					TipoItem.INGRESSO
				),
				ItemFactory.Create(
					 "Ingressos para o concerto do Turisas",
					 10,
					49,
					TipoItem.INGRESSO
				),
				ItemFactory.Create(
					 "Ingressos para o concerto do Turisas",
					 5,
					49,
					TipoItem.INGRESSO
				),
				ItemFactory.Create("Bolo de Mana Conjurado", 3, 6, TipoItem.CONJURADO)
			};

			var app = new GildedRose(itens);

			for (var i = 0; i < 31; i++)
            {
                ImprimirSaida(itens, i);
                app.AtualizarQualidade();
            }
        }

        private static void ImprimirSaida(IList<Item> itens, int i)
        {
            Console.WriteLine("-------- dia " + i + " --------");
            Console.WriteLine("Nome, PrazoParaVenda, Qualidade");
            for (var j = 0; j < itens.Count; j++)
            {
                Console.WriteLine(itens[j].Nome + ", " + itens[j].PrazoParaVenda + ", " + itens[j].Qualidade);
            }
            Console.WriteLine("");
        }
    }
}
