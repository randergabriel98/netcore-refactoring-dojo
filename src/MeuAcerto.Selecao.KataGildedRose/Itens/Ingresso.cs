namespace MeuAcerto.Selecao.KataGildedRose
{
    public class Ingresso : ItemBase {
        public Ingresso(string nome, int prazoParaVenda, int qualidade) : base(nome, prazoParaVenda, qualidade)
        {
        }

        protected override void AtualizarValores() {
            if(this.PrazoParaVenda <= 0) {
                this.Qualidade = 0;
            }
            else if(this.PrazoParaVenda <= 5)
            {
                this.Qualidade+=3;
            }
            else if(this.PrazoParaVenda <= 10)
            {
                this.Qualidade+=2;
            }
            else if(this.PrazoParaVenda > 0)
            {
                this.Qualidade++;
            }
            this.PrazoParaVenda--;
        }
    }
}