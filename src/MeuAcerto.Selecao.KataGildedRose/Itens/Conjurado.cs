namespace MeuAcerto.Selecao.KataGildedRose
{
    public class Conjurado : ItemBase {
        public Conjurado(string nome, int prazoParaVenda, int qualidade) : base(nome, prazoParaVenda, qualidade)
        {
        }

        protected override void AtualizarValores() {           
            if (this.Qualidade > 0)
            {
                if(this.PrazoParaVenda > 0)
                    this.Qualidade-=2;
                else
                    this.Qualidade-=4;
            }
            this.PrazoParaVenda--;
        }
    }
}