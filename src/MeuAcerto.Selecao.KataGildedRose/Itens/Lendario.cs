namespace MeuAcerto.Selecao.KataGildedRose
{
    public class Lendario : ItemBase {
        public Lendario(string nome, int prazoParaVenda, int qualidade) : base(nome, prazoParaVenda, qualidade)
        {
        }
        protected override void TratarLimites() {}
        protected override void AtualizarValores() {}
    }
}