namespace MeuAcerto.Selecao.KataGildedRose
{
    public abstract class ItemBase : Item, IItem {
        public ItemBase(string nome, int prazoParaVenda, int qualidade) {
            Nome = nome;
            PrazoParaVenda = prazoParaVenda;
            Qualidade = qualidade;
        }
        protected virtual void TratarLimites() {
            if(this.Qualidade >= 50) {
                this.Qualidade = 50;
            }
            if(this.Qualidade < 0) this.Qualidade = 0;
        }
        protected abstract void AtualizarValores();

        public void Atualizar()
        {
            AtualizarValores();
            TratarLimites();
        }
    }
}