namespace MeuAcerto.Selecao.KataGildedRose
{
    public class Envelhecido : ItemBase {
        public Envelhecido(string nome, int prazoParaVenda, int qualidade) : base(nome, prazoParaVenda, qualidade)
        {
        }

        protected override void AtualizarValores() {
            if(this.PrazoParaVenda > 0) {
                this.Qualidade++;
            } else {
                this.Qualidade += 2;
            }
            this.PrazoParaVenda--;
        }
    }
}