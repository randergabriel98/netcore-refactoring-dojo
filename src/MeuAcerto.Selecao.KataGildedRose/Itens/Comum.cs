namespace MeuAcerto.Selecao.KataGildedRose
{
    public class Comum : ItemBase {
        public Comum(string nome, int prazoParaVenda, int qualidade): base(nome, prazoParaVenda, qualidade) {
        }
        protected override void AtualizarValores() {
            
            if (this.Qualidade > 0)
            {
                if(this.PrazoParaVenda > 0)
                    this.Qualidade--;
                else
                    this.Qualidade-=2;
            }
            this.PrazoParaVenda--;
        }
    }
}