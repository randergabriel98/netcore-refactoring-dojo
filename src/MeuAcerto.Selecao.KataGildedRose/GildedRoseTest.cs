﻿using System;
using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseTest
    {
        [Fact]
        public void foo()
        {
            IList<Item> Items = new List<Item> { new Comum ("foo", 0, 0) };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("foo", Items[0].Nome);
        }

        [Fact]
        public void test_ItemLendario() {
            ItemBase item = new Lendario("Lendario",10, 10);
            item.Atualizar();
            Assert.Equal(10, item.Qualidade);
            Assert.Equal(10, item.PrazoParaVenda);
        }

        [Theory]
        [InlineData(10, 10, 1, 9)]
        [InlineData(10, 10, 2, 8)]
        [InlineData(10, 1, 2, 7)]
        [InlineData(10, 0, 1, 8)]
        [InlineData(10, 0, 2, 6)]
        public void test_ItemBase(int qualidade, int prazo, int iteracoes, int qualidadeEsperada) {
            ItemBase item = new Comum ("Base", prazo, qualidade);
            for(var i = 0; i < iteracoes; i++)
                item.Atualizar();
            Assert.Equal(qualidadeEsperada, item.Qualidade);
        }
        
        [Theory]
        [InlineData(10, 10, 1, 11)]
        [InlineData(10, 10, 2, 12)]
        [InlineData(10, 0, 1, 12)]
        [InlineData(10, 0, 2, 14)]
        [InlineData(49, 0, 1, 50)]
        [InlineData(49, 0, 10, 50)]
        public void test_QueijoBrieEnvelhecido(int qualidade, int prazo, int iteracoes, int qualidadeEsperada) {
            ItemBase item = new Envelhecido("Queijo", prazo, qualidade);
            for(var i = 0; i < iteracoes; i++)
                item.Atualizar();
            Assert.Equal(qualidadeEsperada, item.Qualidade);
        }

        [Theory]
        [InlineData(10, 10, 1, 12)]
        [InlineData(10, 10, 11, 0)]
        [InlineData(10, 11, 1, 11)]
        [InlineData(10, 5, 1, 13)]
        [InlineData(10, 5, 2, 16)]
        [InlineData(10, 10, 9, 32)]
        [InlineData(49, 10, 1, 50)]
        [InlineData(49, 10, 10, 50)]
        [InlineData(10, 0, 1, 0)]
        public void test_Ingresso(int qualidade, int prazo, int iteracoes, int qualidadeEsperada) {
            ItemBase item = new Ingresso("Ingresso", prazo, qualidade);
            for(var i = 0; i < iteracoes; i++)
                item.Atualizar();
            Assert.Equal(qualidadeEsperada, item.Qualidade);
        }

        [Theory]
        [InlineData(10, 10, 1, 8)]
        [InlineData(10, 10, 2, 6)]
        [InlineData(10, 0, 1, 6)]
        [InlineData(10, 0, 2, 2)]
        [InlineData(10, 0, 6, 0)]
        public void test_Conjurado(int qualidade, int prazo, int iteracoes, int qualidadeEsperada) {
            ItemBase item = new Conjurado("Conjurado", prazo, qualidade);
            for(var i = 0; i < iteracoes; i++)
                item.Atualizar();
            Assert.Equal(qualidadeEsperada, item.Qualidade);
        }

        [Theory]
        [InlineData(TipoItem.COMUM, typeof(Comum))]
        [InlineData(TipoItem.CONJURADO, typeof(Conjurado))]
        [InlineData(TipoItem.ENVELHECIDO, typeof(Envelhecido))]
        [InlineData(TipoItem.LENDARIO, typeof(Lendario))]
        [InlineData(TipoItem.INGRESSO, typeof(Ingresso))]
        public void test_ItemFactory(TipoItem tipoItem, Type tipoEsperado) {
            Item item = ItemFactory.Create("Nome", 10, 10, tipoItem);
            Assert.Equal(item.GetType(), tipoEsperado);
        }
    }
}
