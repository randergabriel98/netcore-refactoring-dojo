namespace MeuAcerto.Selecao.KataGildedRose {
    public enum TipoItem {
        COMUM,
        CONJURADO,
        ENVELHECIDO,
        LENDARIO,
        INGRESSO
    }
    public static class ItemFactory {
        public static Item Create(string nome, int prazoParaVenda, int qualidade, TipoItem tipo) {
            switch (tipo)
            {
                case TipoItem.COMUM:
                    return new Comum(nome, prazoParaVenda, qualidade);
                case TipoItem.CONJURADO:
                    return new Conjurado(nome, prazoParaVenda, qualidade);
                case TipoItem.ENVELHECIDO:
                    return new Envelhecido(nome, prazoParaVenda, qualidade);
                case TipoItem.LENDARIO:
                    return new Lendario(nome, prazoParaVenda, qualidade);
                case TipoItem.INGRESSO:
                    return new Ingresso(nome, prazoParaVenda, qualidade);
                default:
                    throw new System.Exception("Tipo não suportado!");
            }
        }
    }
}